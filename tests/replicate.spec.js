const expect = require("chai").expect;

describe("Address Issues", function() {
    before(function() {
        browser.maximizeWindow()
        browser.url("/g4m/0005");
        $("button.g4m-btn-tertiary").click();
        $("a.add-to-basket-button").waitForDisplayed();
        $("a.add-to-basket-button").click();
        $("#basket-form [type='submit']").waitForDisplayed();
        browser.pause(1000);
        $("#basket-checkout-btn-bottom").click();
        $("#register_form").waitForDisplayed();
        const timestamp = Date.now();
        $("#email_address").setValue(`qa+${timestamp}@gear4music.com`);
        $("#forename").setValue("Ghost");
        $("#lastname").setValue("Inspector");
        $("#telephone").setValue("123456789");
        $(".billing .expand-select a").click();
        $(".billing #address_2").waitForDisplayed();

        $("#postal_code").setValue("WR5 3DA");
        $("#address_1").setValue("Loqate");
        $("#address_2").setValue("Waterside, Basin Road");
        $("#address_3").setValue("Worcester");

        $$(".diff-del-container .expand-select")[0].click();
        $(".diff-del-container #address_1").waitForDisplayed();

        $("#contact_first_name").setValue("Ghost");
        $("#contact_last_name").setValue("Inspector");
        $("#contact_phone").setValue("123456789");

        $(".diff-del-container #postal_code").setValue("M1 3BE");
        $(".diff-del-container #address_1").setValue("Gear4music");
        $(".diff-del-container #address_2").setValue("4th Floor, 1 Portland Street");
        $(".diff-del-container #address_3").setValue("Manchester");
        $("#register_form [type='submit']").click();
    });
    it("Changes the 'Delivery Address' to match the 'Billing Address'", function() {
        $(".change-billing-address").waitForDisplayed();
        $(".change-billing-address").click();
        $("#address-book-container").waitForExist();
        browser.pause(1000);
        $(".set-billing-address").click();
        $("#generic-g4m-modal .overlay-loading").waitForExist(null, true);
        browser.pause(1000);
        $("#address-book-container .overlay-loading").waitForExist(null, true);
        browser.pause(1000);
        $("#address-book-container").waitForExist(null, true);
        browser.pause(1000);
        const billingAddress = $("#payment-billing-address").getText();
        const deliveryAddress = $(" #payment-delivery-address").getText();
        expect(billingAddress).to.contain("M1 3BE");
        expect(deliveryAddress).to.contain("M1 3BE");
    });
});